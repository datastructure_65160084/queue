/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package com.mycompany.queue;

/**
 *
 * @author asus
 */
public class QueueApp {
     public static void main(String[] args) {
        Queue theQueue = new Queue(5); // Queue holds 5 items
        theQueue.insert(10); // Insert 4 items
        theQueue.insert(20);
        theQueue.insert(30);
        theQueue.insert(40);
        theQueue.remove(); // Remove 3 items (10, 20, 30)
        theQueue.remove();
        theQueue.remove();
        theQueue.insert(50); // Insert 4 more items (wraps around)
        theQueue.insert(60);
        theQueue.insert(70);
        theQueue.insert(80);

        while (!theQueue.isEmpty()) {
            // Remove and display all items
            long n = theQueue.remove(); // (40, 50, 60, 70, 80)
            System.out.print(n + " ");
        }
        System.out.println(); // Print a newline after all items are displayed
    } // end main()
}
